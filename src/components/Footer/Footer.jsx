import React from 'react';
import classes from './Footer.module.scss';
import { FooterElement } from './FooterElement';
import Call from '../../assets/img/icons/call.png';
import Envelope from '../../assets/img/icons/envelope.png';
import Home from '../../assets/img/icons/home.png';
import Facebook from '../../assets/img/icons/facebook.png';
import Instagram from '../../assets/img/icons/instagram.png';

export const Footer = (props) => (
    <div className={classes.Footer}>
        <FooterElement FooterelementIcon={Call} FooterelementText={"(8-645) 22807"} alt="call"/>
        <FooterElement FooterelementIcon={Home} FooterelementText={"Ateities g. 31B, Vilnius, Lietuva"} alt="home"/>
        <FooterElement FooterelementIcon={Envelope} FooterelementText={"vaivar@yahoo.com"} alt="envelope"/>
        <FooterElement FooterelementIcon={Facebook} FooterelementText={"Facebook"} alt="facebook"/>
        <FooterElement FooterelementIcon={Instagram} FooterelementText={"Instagram"} alt="instagram"/>
    </div>
);