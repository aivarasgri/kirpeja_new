import React from 'react';
import classes from './FooterElement.module.scss';

export const FooterElement = (props) => (
    <div className={classes.FooterElement}>
        <div className={classes.FooterElementInfoContainer}>
            <img src={props.FooterelementIcon} alt={props.alt}/>
            <span>{props.FooterelementText}</span>
        </div>   
    </div>
);
