import React from 'react';
import classes from './Backdrop.module.scss';

export const Backdrop = (props) => (
    <div onClick={props.clickBackdrop} 
        className={classes.Backdrop}>
    </div>
);