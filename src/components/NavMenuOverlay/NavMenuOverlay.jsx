import React from 'react';
import classes from './NavMenuOverlay.module.scss';

export const NavMenuOverlay = (props) => {

    let drawerClasses = [classes.NavMenuOverlay];
    if ( props.show ) {
        drawerClasses = [classes.NavMenuOverlay, classes.Open];
    } 

   return (
        <div className={drawerClasses.join(' ')}>
            <div className={classes.closeMenuOverlayBtn} 
               onClick={props.closeMenuBtn}>
            </div>
            <ul>
                <li><a href="/">KAINOS</a></li>
                <li><a href="/">GALERIJA</a></li>
                <li><a href="/">KONTAKTAI</a></li>
            </ul>
        </div>
    );
}