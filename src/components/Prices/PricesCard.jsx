import React  from 'react';
import classes from './PricesCard.module.scss';

export const PricesCard = (props) => (
    <div className={classes.PriceCard} >
        <img className={classes.CardImage} src={props.ImgSrc} alt="haircut" />
        <h4>{props.title}</h4>
        <ul>
            <li><span>{props.service1}</span><span>{props.price1}</span></li>
            <li><span>{props.service2}</span><span>{props.price2}</span></li>
            <li><span>{props.service3}</span><span>{props.price3}</span></li>
            <li><span>{props.service4}</span><span>{props.price4}</span></li>
        </ul> 
    </div> 
);
