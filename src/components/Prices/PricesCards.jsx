import React from 'react';
import {PricesCard} from './PricesCard';
import classes from './PricesCards.module.scss';
import img1 from  '../../assets/img/haircut.jpg';
import img2 from  '../../assets/img/coloring.png';
import img3 from  '../../assets/img/salon.jpg';

export const UseOnScreen = (options) => {
    const ref = React.useRef();
    const [visible, setVisible] = React.useState(false);

    React.useEffect(() => {
        const observer = new IntersectionObserver(([entry]) => {
            setVisible(entry.isIntersecting);
        }, options);

        if( ref.current ) {
            observer.observe(ref.current);
        }

        return () => {
            if( ref.current ) {
                observer.unobserve(ref.current);
                ref.current = null;
            }
        };

    }, [ref, options]);

    return [ref, visible];
}

export const Prices = (props) => {

const [ref, visible] = UseOnScreen({ threshold: 0.5 });

return (
     <div className={classes.PricesWrapper} ref={ref}>
       { visible ?  ( 
        <>
        <h2 className={classes.PriceHeader}>KAINOS</h2>
            <div className={classes.CardContainer}>
                <>
                    <PricesCard 
                    ImgSrc={img1}
                    title="KIRPIMAI"
                    price1="nuo 40eur"
                    price2="nuo 20eur"
                    service1="Karstomis zirklemis"
                    service2="Moteriskas"
                    />
                </>
                <>
                    <PricesCard 
                    ImgSrc={img2}
                    title="DAZYMAI"
                    price1="nuo 50eur"
                    price2="nuo 30eur"
                    price3="nuo 40eur"
                    price4="nuo 30eur"
                    service1="Balayage technikomis"
                    service2="Sruogelėmis"
                    service3="Per visa ilgi"
                    service4="Šaknys"
                    />
                </>
                <>
                    <PricesCard 
                    ImgSrc={img3}
                    title="PROCEDUROS"
                    price1="40 eur"
                    price2="25 eur"
                    service1="Drėkinanti ar maitinanti"
                    service2="Plauku rekonstrukcine"/>
                </>
            </div>
        </>  ) : null }
     </div>
    );
};