import React from 'react';
import classes from './GaleryImage.module.scss';

export const GaleryImage = (props) => (
    <div className={classes.GaleryImageWrapper}>
        <img className={classes.GaleryImage} 
        src={props.ImgSrc} 
        alt="GaleryImg"/>
    </div>
);