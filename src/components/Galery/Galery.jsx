import React from 'react';
import classes from './Galery.module.scss';
import { GaleryImage } from '../Galery/GaleryImage/GaleryImage';
import Work2 from '../../assets/img/work/2.jpg';
import Work3 from '../../assets/img/work/3.jpg';
import Work4 from '../../assets/img/work/4.jpg';
import Work5 from '../../assets/img/work/5.jpg';
import Work6 from '../../assets/img/work/6.jpg';
import Work7 from '../../assets/img/work/7.jpg';
import Work8 from '../../assets/img/work/8.jpg';
import Work9 from '../../assets/img/work/9.jpg';
import Work11 from '../../assets/img/work/11.jpg';
import Work12 from '../../assets/img/work/12.jpg';
import Work13 from '../../assets/img/work/13.jpg';
import Work15 from '../../assets/img/work/15.jpg';

// import { UseOnScreen } from '../Prices/PricesCards';

// const UseOnScreen = (options) => {
//   const ref = React.useRef();
//   const [visible, setVisible] = React.useState(false);

//   React.useEffect(() => {
//       const observer = new IntersectionObserver(([entry]) => {
//           setVisible(entry.isIntersecting);
//       }, options);

//       if( ref.current ) {
//           observer.observe(ref.current);
//       }

//       return () => {
//           if( ref.current ) {
//               observer.unobserve(ref.current);
//               ref.current = null;
//           }
//       };

//   }, [ref, options]);

//   return [ref, visible];
// }
export const Galery = (props) => {

  // const [ref, visible] = UseOnScreen({ threshold: 0.25 });

  return (
   <div className={classes.GaleryWrapper}
      // ref={ref} 
      // style={{marginBottom: '100px'}}
      >
   
    <>
      <h2 className={classes.GaleryHeader}>GALERIJA</h2>
      {/* { visible ?  */}
      <div className={classes.GaleryContainer} >
        <div className={classes.GaleryColumn} >
          <GaleryImage ImgSrc={Work11}/>
          <GaleryImage ImgSrc={Work2}/>
          <GaleryImage ImgSrc={Work3}/>
        </div>
        <div className={classes.GaleryColumn} >
          <GaleryImage ImgSrc={Work8}/>
          <GaleryImage ImgSrc={Work5}/>
          <GaleryImage ImgSrc={Work6}/>
        </div>
        <div className={classes.GaleryColumn} >
          <GaleryImage ImgSrc={Work7}/>
          <GaleryImage ImgSrc={Work4}/>
          <GaleryImage ImgSrc={Work15}/>
        </div>
        <div className={classes.GaleryColumn} >
          <GaleryImage ImgSrc={Work13}/>
          <GaleryImage ImgSrc={Work9}/>
          <GaleryImage ImgSrc={Work12}/>
        </div>        
      </div> 
      {/* : null } */}
    </> 
   </div>
  );
}; 

