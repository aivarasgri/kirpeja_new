import React from 'react';
import classes from './Navigation.module.scss';
import navMenu from '../../assets/img/menuwhite.png';
import Logo from '../../assets/img/scissorswhite.png';
import Scroll from '../../assets/img/scroll.png';

export const Navigation = (props) => (
    <header>
        <nav className={classes.Navigation}>
          <h1 className={classes.HeaderTitle} >
              <span className={classes.HeaderTitlePrimary} >KIRPEJA</span>
              <span className={classes.HeaderTitleSecondary} >VAIVA</span>
          </h1>
            <div>
                <img className={classes.Logo} 
                src={Logo} 
                alt="logo" />
            </div>
            <div onClick={props.toggleOverlayMenuHandler}>
                <img className={classes.NavMenu} 
                src={navMenu} 
                alt="navMenu" />
            </div> 
             <img src={Scroll} alt="scroll" className={classes.ScrollIcon}/>
        </nav>
       
    </header>    
);
