import React, { useState } from 'react';
// import './App.css';
import { Navigation } from './components/Navigation/Navigation';
import { NavMenuOverlay } from './components/NavMenuOverlay/NavMenuOverlay';
import { Backdrop } from  './components/Backdrop/Backdrop';
import { Prices } from './components/Prices/PricesCards';
import { Galery } from './components/Galery/Galery';
import { Footer } from './components/Footer/Footer';

const App = () => {
  
  const [toggle, setToggle] = useState(false);

  const overlayToggleClickHandler = () => {
    setToggle((prev) => {
      return {toggle: !prev.toggle};
    });
  }

  const backdropToggleHandler = () => {
    setToggle(false);
  }

  const closeOverlayMenuButton = () => {
    setToggle(false);
  }

  // const onScrollTrigerPriceCardAnimationHandler = () => {

  // }

  let backdrop;

  if ( toggle ) {
    backdrop = <Backdrop clickBackdrop={backdropToggleHandler}/>
  }

  return (
    <>
      <Navigation toggleOverlayMenuHandler={overlayToggleClickHandler}/>
      <NavMenuOverlay show={toggle} closeMenuBtn={closeOverlayMenuButton}/>  
      {backdrop}
      <Prices />  
      <Galery />
      <Footer />
    </>
  );
}

export default App;
